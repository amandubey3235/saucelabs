package pagedesign.test;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageDesign.pageObejct.cartItems;
import pageDesign.pageObejct.fillYourInformation;
import pagedesign.Testcomponents.BaseClass;

public class orderCheckout extends BaseClass {

	@Test(description="In and Out Scenario from login to logout")
	public void checoutPage() throws IOException {

		// Login to the Websites
		hmPage.imlicitwait();
		cartItems cart = hmPage.loginToPage("standard_user", "secret_sauce");
		// Verify Item page
		AssertJUnit.assertEquals(cart.verifyordersPage(), "Products");
		// Select Items
		String items[] = { "Sauce Labs Backpack", "Sauce Labs Bike Light", "Sauce Labs Onesie" };
		cart.getProductList(items);
		cart.gotoCartPage();
		AssertJUnit.assertEquals(cart.getItemName(), "Sauce Labs Backpack");
		cart.clickOnCheckout();
		AssertJUnit.assertEquals(cart.verifyInformationPage(), "Checkout: Your Information");
		fillYourInformation information = cart.goToInformationPage();
		// Personal information Page
		String mydata[] = { "Mark", "wendy", "002233" };
		List<String> fillData = Arrays.asList(mydata);
		information.personalInfo(fillData);
		AssertJUnit.assertEquals(information.verifyOrderDetails(), "Thank you for your order!");
		information.verifyOrderSuccess();
		information.verifyOrderConfirmation();
		AssertJUnit.assertEquals(cart.verifyordersPage(), "Products");
		hmPage.logout();
		
	}
	
	@Test(description="Edit my Cart Items")
	public void editCartItem() {
		
		hmPage.imlicitwait();
		cartItems cart = hmPage.loginToPage("standard_user", "secret_sauce");
		// Verify Item page
		AssertJUnit.assertEquals(cart.verifyordersPage(), "Products");
		// Select Items
		String items[] = { "Sauce Labs Backpack", "Sauce Labs Bike Light", "Sauce Labs Onesie" };
		cart.getProductList(items);
		cart.gotoCartPage();
		cart.removeItem("Sauce Labs Bike Light");
		cart.continueShoping();
		cart.gotoCartPage();
		AssertJUnit.assertEquals(cart.getItemName(), "Sauce Labs Backpack");
		cart.clickOnCheckout();
		fillYourInformation information = cart.goToInformationPage();
		// Personal information Page
		String mydata[] = { "Mark", "wendy", "002233" };
		List<String> fillData = Arrays.asList(mydata);
		information.personalInfo(fillData);
		information.verifyOrderSuccess();
		information.verifyOrderConfirmation();
	}
	
	@Test(description="Filter the Items then select a Product",dataProvider="getData",groups= {"Sanity"})
	public void filterListedItems(HashMap<String,String> loginData) {
		hmPage.imlicitwait();
		cartItems cart = hmPage.loginToPage(loginData.get("user"),loginData.get("password"));
		// Verify Item page
		AssertJUnit.assertEquals(cart.verifyordersPage(), "Products");
		//Price (low to high), Price (high to low), Name (Z to A)
		hmPage.filterItem("Price (high to low)");
		AssertJUnit.assertEquals(hmPage.checkCurrentFilter(), "Price (high to low)");
		cart.gotoCartPage();
		cart.clickOnCheckout();
		fillYourInformation information = cart.goToInformationPage();
		// Personal information Page
		String mydata[] = { "Mark", "wendy", "002233" };
		List<String> fillData = Arrays.asList(mydata);
		information.personalInfo(fillData);
		information.verifyOrderSuccess();
		information.verifyOrderConfirmation();
	}
	
	@DataProvider
	public Object[][] getData() throws IOException{
		List<HashMap<String,String>> data=getJsonData(System.getProperty("user.dir")+"/src/main/java/pageDesign/resources/Users.json");
		return new Object[][] {{data.get(0)},{data.get(1)}};
	}
	
}
