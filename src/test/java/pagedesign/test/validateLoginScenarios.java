package pagedesign.test;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.Assert;
import org.testng.annotations.Test;

import pagedesign.Testcomponents.BaseClass;
import pagedesign.Testcomponents.RetryAnalyzer;

public class validateLoginScenarios extends BaseClass{

	@Test(description= "Username is blank",groups= {"ErrorValidation"})
	public void verifyAttempts() {
		hmPage.loginToPage("", "standard_user");
		String errorMessage=hmPage.failedAttempts();
		System.out.println(errorMessage);
		AssertJUnit.assertEquals(hmPage.failedAttempts(),"Epic sadface: Username is required");
		
	}
	@Test(description="Password is blank", retryAnalyzer=RetryAnalyzer.class)
	public void verifyBlankPasswordAttempts() {
		hmPage.loginToPage("standard_user", "");
		String errorMessage=hmPage.failedAttempts();
		System.out.println(errorMessage);
		AssertJUnit.assertEquals(hmPage.failedAttempts(),"Epic sadface: Password is");
	}
	@Test(description="Wrong Password")
	public void verifyWrongPasswordAttempts() {
		hmPage.loginToPage("standard_user", "secret_sauc");
		String errorMessage=hmPage.failedAttempts();
		System.out.println(errorMessage);
		AssertJUnit.assertEquals(hmPage.failedAttempts(),"Epic sadface: Username and password do not match any user in this service");
	}
	@Test(description="Wrong User")
	public void verifyWrongUserAttempts() {
		hmPage.loginToPage("standard_us", "secret_sauce");
		String errorMessage=hmPage.failedAttempts();
		System.out.println(errorMessage);
		AssertJUnit.assertEquals(hmPage.failedAttempts(),"Epic sadface: Username and password do not match any user in this service");
		
	}
	
	@Test(description="Both User & Password is wrong filled")
	public void verifyInavildAttempts() {
		hmPage.loginToPage("standard_us", "secret_sauc");
		String errorMessage=hmPage.failedAttempts();
		System.out.println(errorMessage);
		AssertJUnit.assertEquals(hmPage.failedAttempts(),"Epic sadface: Username and password do not match any user in this service");
		
		
	}
	
}
