package pagedesign.Testcomponents;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import pageDesign.pageObejct.homePage;

public class BaseClass {

	public WebDriver driver;
	public homePage hmPage;
	public WebDriver intializeDriver() throws IOException {

		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"/src/main/java/pageDesign/resources/GlobalData.properties");
		prop.load(fis);
		String getBrowser =System.getProperty("browser")!=null ? System.getProperty("browser") :prop.getProperty("browser");
		if (getBrowser.equalsIgnoreCase("chrome")) {
			driver = new ChromeDriver();

		} else if (getBrowser.equalsIgnoreCase("firefox")) {
			driver = new FirefoxDriver();

		} else if (getBrowser.equalsIgnoreCase("edge")) {
			driver = new EdgeDriver();

		} else {
			System.out.println("Please check your browser value");
		}

		driver.manage().window().maximize();
		return driver;

	}
	public List<HashMap<String, String>> getJsonData(String FilePath) throws IOException {
		String jsonFile=FileUtils.readFileToString(new File(FilePath));
		
		ObjectMapper mapper=new ObjectMapper();
		List<HashMap<String,String>> data=mapper.readValue(jsonFile, new TypeReference<List<HashMap<String,String>>>(){
			
		});
		return data;
	}
	
	public String getScreenshot(String testCaseName,WebDriver driver) throws IOException {
		
		TakesScreenshot ts=(TakesScreenshot)driver;
		File source=ts.getScreenshotAs(OutputType.FILE);
		File file=new File(System.getProperty("user.dir")+"//reports//"+testCaseName+".png");
		FileUtils.copyFile(source, file);
		
		return System.getProperty("user.dir")+"//reports//"+testCaseName+".png";
		
	}
	@BeforeMethod(alwaysRun=true)
	public homePage luanchBrowser() throws IOException {
		
		intializeDriver();
		hmPage=new homePage(driver);
		hmPage.gotoWebsite();
		return hmPage;
	}
	
	@AfterMethod(alwaysRun=true)
	public void tearDown() {
		driver.close();
	}

}
