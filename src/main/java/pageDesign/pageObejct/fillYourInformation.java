package pageDesign.pageObejct;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import pageDesign.utility.reUsableCodes;

public class fillYourInformation extends reUsableCodes {

	WebDriver driver;

	public fillYourInformation(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "#first-name")
	WebElement firstName;

	@FindBy(css = "#last-name")
	WebElement lastName;

	@FindBy(css = "input[placeholder='Zip/Postal Code']")
	WebElement postalCode;

	@FindBy(css = "input[value='Continue']")
	WebElement moveFruthor;

	@FindBy(css = "#finish")
	WebElement finish;

	@FindBy(xpath = "//h2[text()='Thank you for your order!']")
	WebElement orderConfirmationText;

	@FindBy(xpath = "//div[@class='complete-text']")
	WebElement orderSuccess;
	String odermessage;
	
	@FindBy(css="#back-to-products")
	WebElement goToMenu;
	
	public String personalInfo(List<String> fillData) {

		firstName.sendKeys(fillData.get(0));
		lastName.sendKeys(fillData.get(1));
		postalCode.sendKeys(fillData.get(2));
		moveFruthor.click();
		finish.click();
		return orderConfirmationText.getText();
	}

	public String verifyOrderDetails() {

		return orderConfirmationText.getText();
	}

	public String verifyOrderSuccess() {
		  odermessage=orderSuccess.getText();
		 
		 return odermessage;
	}
	
	public void verifyOrderConfirmation() {
		if (odermessage.equalsIgnoreCase(
				"Your order has been dispatched, and will arrive just as fast as the pony can get there!")) {
			System.out.println(odermessage);
			goToMenu.click();
		}
	}
}
