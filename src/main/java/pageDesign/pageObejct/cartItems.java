package pageDesign.pageObejct;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pageDesign.utility.reUsableCodes;

public class cartItems extends reUsableCodes {
	WebDriver driver;

	public cartItems(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[@class=\"inventory_item_name\"]")
	List<WebElement> ItemsList;

	@FindBy(css = "span[class=\"title\"]")
	WebElement pageLocation;

	@FindBy(xpath = "//a[@class=\"shopping_cart_link\"]")
	WebElement viewCart;
	@FindBy(xpath = "//div[text()=\"Sauce Labs Backpack\"]")
	WebElement getItemName;

	@FindBy(css = "#checkout")
	WebElement clickCheckout;

	@FindBy(xpath = "//span[text()='Checkout: Your Information']")
	WebElement informationDisplay;
	
	@FindBy(xpath="//div[@class='cart_item']")
	List<WebElement> cartItems;
	
	@FindBy(css = "#continue-shopping")
	WebElement continueShopings;

	public String verifyordersPage() {

		return pageLocation.getText();
	}

	public void getProductList(String listOfItems[]) {

		List<String> Items = Arrays.asList(listOfItems);

		int count = 1;
		for (int i = 0; i < ItemsList.size(); i++) {
			if (Items.contains(ItemsList.get(i).getText())) {
				driver.findElement(By.xpath("//div[@class='inventory_list']/div[" + count
						+ "]/div[2]/div[2]/div/following-sibling::button")).click();
			}
			count++;
		}
	}

	public void gotoCartPage() {
		viewCart.click();
	}

	public String getItemName() {
		return getItemName.getText();
	}

	public void clickOnCheckout() {

		clickCheckout.click();

	}

	public String verifyInformationPage() {

		return informationDisplay.getText();
	}
	
	public void removeItem(String removeItem) {
		int count=3;
		for(int i=0;i<cartItems.size();i++) {
			if(cartItems.get(i).getText().equalsIgnoreCase(removeItem)) {
				driver.findElement(By.xpath("//div[@class=\"cart_list\"]/div["+count+"]/div[2]/div[2]/button")).click();
			}
			count++;
		}
		
	}
	public void continueShoping() {
		continueShopings.click();
	}

	public fillYourInformation goToInformationPage() {

		fillYourInformation information = new fillYourInformation(driver);

		return information;
	}

}
