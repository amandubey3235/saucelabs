package pageDesign.pageObejct;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import pageDesign.utility.reUsableCodes;

public class homePage extends reUsableCodes {

	WebDriver driver;

	public homePage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@placeholder='Username']")
	WebElement userEmail;

	@FindBy(css = "input[type='password']")
	WebElement userPassword;

	@FindBy(css = "#login-button")
	WebElement login;

	@FindBy(xpath = "//h3[@data-test='error']")
	WebElement failedAttempt;

	@FindBy(css = "#react-burger-menu-btn")
	WebElement menu;

	@FindBy(xpath = "//nav[@class='bm-item-list']/a")
	List<WebElement> homeMenu;

	@FindBy(xpath = "//select[@class='product_sort_container']")
	WebElement filter;

	// Validate selected Filter

	@FindBy(xpath = "//span[@class='active_option']")
	WebElement currentFilter;
	// filter locators

	@FindBy(xpath = "//div[@class='pricebar']/div")
	List<WebElement> listPrice;

	By failedWait = By.xpath("//h3[@data-test='error']");

	public void gotoWebsite() {
		driver.get("https://www.saucedemo.com/");
	}

	public cartItems loginToPage(String email, String password) {

		userEmail.sendKeys(email);
		userPassword.sendKeys(password);
		login.click();
		cartItems cart = new cartItems(driver);
		return cart;
	}

	public String failedAttempts() {

		exlpicitWaitConditions(failedWait);
		return failedAttempt.getText();
	}

	public void logout() {
		menu.click();
		for (int i = 0; i < homeMenu.size(); i++) {
			if (homeMenu.get(i).getText().equalsIgnoreCase("Logout")) {
				driver.findElement(By.xpath("//nav[@class='bm-item-list']/a[" + i + "]")).click();
				System.out.println("User as Loged Out Successfully");
			}
		}
	}

	public void filterItem(String filtertype) {

		Select sc = new Select(filter);
		sc.selectByVisibleText(filtertype);
		int count = 1;
		if (filtertype.equalsIgnoreCase("Price (low to high)")) {
			for (int i = 0; i < listPrice.size(); i++) {

				String get = listPrice.get(i).getText();
				String[] getPrice = get.split("\\$");
				double price = Double.parseDouble(getPrice[1]);
				if (price <= 8) {
					System.out.println("Product Price: " + listPrice.get(i).getText());
					driver.findElement(By.xpath("//div[@class='inventory_list']/div[" + count
							+ "]/div[2]/div[2]/div/following-sibling::button")).click();
				}
				count++;
			}
		} else if (filtertype.equalsIgnoreCase("Price (high to low)")) {
			for (int i = 0; i < listPrice.size(); i++) {

				String get = listPrice.get(i).getText();
				String[] getPrice = get.split("\\$");
				double price = Double.parseDouble(getPrice[1]);
				if (price <= 50) {
					System.out.println("Product Price: " + listPrice.get(i).getText());
					driver.findElement(By.xpath("//div[@class='inventory_list']/div[" + count
							+ "]/div[2]/div[2]/div/following-sibling::button")).click();
				}
				count++;
			}
		} else if (filtertype.equalsIgnoreCase("Name (Z to A)")) {
			for (int i = 0; i < listPrice.size(); i++) {

				String get = listPrice.get(i).getText();
				String[] getPrice = get.split("\\$");
				double price = Double.parseDouble(getPrice[1]);
				if (price <= 22) {
					System.out.println("Product Price: " + listPrice.get(i).getText());
					driver.findElement(By.xpath("//div[@class='inventory_list']/div[" + count
							+ "]/div[2]/div[2]/div/following-sibling::button")).click();
				}
				count++;
			}
		} else {
			System.out.println("In-correct Value");
		}
	}

	public String checkCurrentFilter() {

		return currentFilter.getText();

	}

}
