package pageDesign.utility;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class reUsableCodes {

	 WebDriver driver;

	public reUsableCodes(WebDriver driver) {
		this.driver=driver;
	}

	public void exlpicitWaitConditions(By explicitlocator) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(6));
		wait.until(ExpectedConditions.visibilityOfElementLocated(explicitlocator));
		

	}
	
	public void imlicitwait() {
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
	}
	
	public void windowSettings() {
		driver.manage().window().maximize();

	}
}
